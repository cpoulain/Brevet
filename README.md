# Bases exercices Brevet

### Avec tous mes remerciements à Jean-Michel Sarlat.

Vous trouverez ici des énoncés d'exercices tirés des sujets de Brevet de 1996 à 2020. C'est, en fait, la transcription sur la forge des bases Brevet de [melusine](https://melusine.eu.org/lab/brevet) et un complément.

ATTENTION: les pages peuvent être longues à télécharger, elles embarquent les images des documents composés. Pour chaque image, le source <u>LaTeX</u> est proposé (via un clic sur l'image) ainsi que le source <u>Metapost</u> (si nécessaire, via un clic sur <img src=Base-sty/b-mp.png>).

Cette <i>mise en ligne</i> est encore *expérimentale* et en cours.

Les documents présentés ici ont tous été préparés par <b>Christophe Poulain</b> — dont certains à partir de sources composées par Denis Le Fur, Denis Vergès et Sébastien Lozano (pour les sources de 2013 à 2020), qu'ils en soient remerciés — et sont <i>librement exploitables</i> dans un usage personnel.

Compte tenu du travail que cela représente, l'exploitation à des fins commerciales ou dans le but de prodéder à <i>une mise en ligne</i> nécessite l'autorisation de l'auteur.

Pour la compilation des fichiers présentés ici, et même si la plupart d'entre eux, malgré un âge certain, compilent encore ; il est conseillé d'utiliser tout de même le package [ProfCollege](http://ctan.org/pkg/profcollege/).

Pour tout souci, demande, repérage de coquilles... contactez <b>Christophe Poulain</b>.

Bonne déambulation à travers les README.md...



 La base comporte 2987 fichiers sources <u>LaTeX</u>.

+ [Année 2023.](2023) (49 fichier(s))
+ [Année 2022.](2022) (70 fichier(s))
+ [Année 2021.](2021) (60 fichier(s))
+ [Année 2020.](2020) (46 fichier(s))
+ [Année 2019.](2019) (164 fichier(s))
+ [Année 2018.](2018) (142 fichier(s))
+ [Année 2017.](2017) (152 fichier(s))
+ [Année 2016.](2016) (118 fichier(s))
+ [Année 2015.](2015) (174 fichier(s))
+ [Année 2014.](2014) (162 fichier(s))
+ [Année 2013.](2013) (156 fichier(s))
+ [Année 2012.](2012) (86 fichier(s))
+ [Année 2011.](2011) (87 fichier(s))
+ [Année 2010.](2010) (62 fichier(s))
+ [Année 2009.](2009) (74 fichier(s))
+ [Année 2008.](2008) (74 fichier(s))
+ [Année 2007.](2007) (48 fichier(s))
+ [Année 2006.](2006) (78 fichier(s))
+ [Année 2005.](2005) (80 fichier(s))
+ [Année 2004.](2004) (132 fichier(s))
+ [Année 2003.](2003) (87 fichier(s))
+ [Année 2002.](2002) (91 fichier(s))
+ [Année 2001.](2001) (90 fichier(s))
+ [Année 2000.](2000) (198 fichier(s))
+ [Année 1999.](1999) (129 fichier(s))
+ [Année 1998.](1998) (95 fichier(s))
+ [Année 1997.](1997) (133 fichier(s))
+ [Année 1996.](1996) (150 fichier(s))
